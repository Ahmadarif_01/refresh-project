import axios from 'axios';
import { config } from '../Config/config';

export const surveiService = {
    save: (model) => {
        const result = axios
          .post(config.apiUrl + "/create_survei", model)
          .then((respons) => {
            return {
              success: respons.data.success,
              result: respons.data.result,
            };
          })
          .catch((error) => {
            return {
              succes: false,
              result: error,
            };
          });
        return result;
      },

      getStatus: () => {
        const result = axios.get(config.apiUrl + '/get_status')
    
          .then(respons => {
            return {
              success: respons.data.success,
              result: respons.data.result
            }
          })
          .catch(error => {
            return {
              success: false,
              result: error
            }
          });
        // //console.log(result);
        return result;
      },

      getList: () => {
        const result = axios.get(config.apiUrl + '/get_survei_list')
    
          .then(respons => {
            return {
              success: respons.data.success,
              result: respons.data.result
            }
          })
          .catch(error => {
            return {
              success: false,
              result: error
            }
          });
        // //console.log(result);
        return result;
      },

      getbyid: (id) => {
        const result = axios
          .get(config.apiUrl + "/getdata/" + id)
          .then((respons) => {
            return {
              success: respons.data.success,
              result: respons.data.result,
            };
          })
          .catch((error) => {
            return {
              succes: false,
              result: error,
            };
          });
        return result;
      },

      delete: (item) => {
        const result = axios.put(config.apiUrl + '/delete_survei/' + item.id)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
      
        return result;
      },

      getFakultas: () => {
        const result = axios.get(config.apiUrl + '/get_fakultas')
    
          .then(respons => {
            return {
              success: respons.data.success,
              result: respons.data.result
            }
          })
          .catch(error => {
            return {
              success: false,
              result: error
            }
          });
        // //console.log(result);
        return result;
      },

      getTP: () => {
        const result = axios.get(config.apiUrl + '/get_transportasi')
    
          .then(respons => {
            return {
              success: respons.data.success,
              result: respons.data.result
            }
          })
          .catch(error => {
            return {
              success: false,
              result: error
            }
          });
        // //console.log(result);
        return result;
      },

      getKota: () => {
        const result = axios.get(config.apiUrl + '/get_kota')
    
          .then(respons => {
            return {
              success: respons.data.success,
              result: respons.data.result
            }
          })
          .catch(error => {
            return {
              success: false,
              result: error
            }
          });
        // //console.log(result);
        return result;
      },
}

export default surveiService;