import React from 'react';
import { Link } from 'react-router-dom';

export default class Sidebar extends React.Component{
    render(){
        return (
            <aside class="main-sidebar">
           
            <section class="sidebar">
        
              <ul class="sidebar-menu" data-widget="tree">
                <li>
                  <a href="/Survei">
                    <i class="fa fa-th"></i> <span>Survei</span>
                  </a>
                </li>
              </ul>
            </section>
          </aside>
        )
    }
}