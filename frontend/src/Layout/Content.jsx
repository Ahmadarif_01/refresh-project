import React from 'react';
import Sidebar from '../Layout/Sidebar';
import Header from '../Layout/Header';
import {Route, Switch } from 'react-router-dom';
import Survei from '../Survei';



function App() {
    return (
      <div className="wrapper">
        <Header />
        <Sidebar />
        <div className="content-wrapper">
  
          <section className="content">
            <Switch>
            <Route exact path="/Survei" component={Survei} />
            </Switch>
            </section>
        </div>
      </div>
    )
  }
  
  export default App;
  