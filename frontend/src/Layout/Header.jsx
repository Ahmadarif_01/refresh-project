import React from 'react';

export default class Header extends React.Component {
    render() {
        return (
            <header class="main-header">

                <a class="logo">

                    <span class="logo-lg"><b>Survei</b> COVID-19</span>
                </a>

                <nav class="navbar navbar-static-top">
                    <div class="navbar">

                    </div>
                </nav>
            </header>
        )
    }
}