import React from "react";
import Content from "./Layout/Content";
import { BrowserRouter, Link, Route, Switch } from "react-router-dom";
import "./App.css";

function App() {
  return (
    <div>
      <BrowserRouter>
        <div className="content-warpper">
          <Switch>
            {/* <Route exact path="/" component={Login} /> */}
            <Content />
          </Switch>
        </div>
      </BrowserRouter>
    </div>
  );
}

export default App;
