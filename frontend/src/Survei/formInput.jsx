import React from "react";
import Modal from "react-bootstrap/Modal";
import "bootstrap/dist/css/bootstrap.min.css";

class FormInput extends React.Component {
  render() {
    const {
      cancel,
      open_create,
      list_status,
      list_fakultas,
      list_kota,
      list_tp,
      model,
      changeHandler,
      selectHandler_fakultas,
      selectHandler_status,
      onSave
    } = this.props;

    return (
      <Modal show={open_create} style={{ opacity: 1 }}>
        <Modal.Header style={{ backgroundColor: "lightblue" }}>
          <Modal.Title>Survei</Modal.Title>
        </Modal.Header>
        {JSON.stringify(model)}
        <br />
        <Modal.Body>
          <form>
            <div className="row">
              <div className="col-md-6">
                <small>Nama Lengkap *</small>
                <input
                  type="text"
                  class="form-control"
                  onChange={changeHandler("nama")}
                />
              </div>
              <div class="col-md-6">
                <small>Email *</small>
                <input
                  type="text"
                  class="form-control"
                  onChange={changeHandler("email")}
                />
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <small>Umur *</small>
                <input
                  type="text"
                  class="form-control"
                  onChange={changeHandler("umur")}
                />
              </div>
              <div class="col-md-6">
                <small>No Handphone *</small>
                <input
                  type="text"
                  class="form-control"
                  onChange={changeHandler("no_hp")}
                />
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <small>Status *</small>
                <select
                  class="form-control"
                  onChange={selectHandler_status("status_id")}
                >
                  <option disabled selected>
                    Pilih Status
                  </option>
                  {list_status.map((data) => {
                    return <option value={data.id}>{data.status}</option>;
                  })}
                </select>
              </div>
              <div class="col-md-6">
                <small>Fakultas *</small>
                <select
                  class="form-control"
                  onChange={selectHandler_fakultas("fakultas_id")}
                >
                  <option disabled selected>
                    Pilih Fakultas
                  </option>
                  {list_fakultas.map((data) => {
                    return <option value={data.id}>{data.fakultas}</option>;
                  })}
                </select>
              </div>
            </div>
            <hr />
            <div class="form-row">
              <div class="col">
                <small>Keluharan *</small>
                <input
                  type="text"
                  class="form-control"
                  onChange={changeHandler("kelurahan")}
                />
              </div>
              <div class="col">
                <small>Kecamatan *</small>
                <input
                  type="text"
                  class="form-control"
                  onChange={changeHandler("kecamatan")}
                />
              </div>
              <div class="col">
                <small>Kabupaten *</small>
                <input
                  type="text"
                  class="form-control"
                  onChange={changeHandler("kabupaten")}
                />
              </div>
            </div>
            <hr />
            <p>Pernyataan</p>
            <label>
              Form ini diisi berdasarkan kesukarelaan dan tidak terdapat
              paksaan. Dengan berpartisipasi mengisi form ini, anda menyatakan
              bersedia bahwa info kontak yang disediakan dapat dihubungi lebih
              lanjut oleh Tim Tanggap Pandemi Covid-19 Unsoed apabila dianggap
              perlu. Semua informasi bersifat rahasia, dikumpulkan serta
              dianalisis untuk kepentingan kesiapsiagan Unsoed menghadapi
              perkembangan infeksi Covid-19. Berikut ini saya menyatakan : *
            </label>
            <div class="form-group">
              <div class="radio">
                <label>
                  <input
                    type="radio"
                    value="Bersedia"
                    id="optionRadios1"
                    name="optionsRadios"
                    onChange={changeHandler("pernyataan_1")}
                  />
                 <b>Bersedia</b>
                </label>
              </div>
              <div class="radio">
                <label>
                  <input
                    type="radio"
                    value="Tidak Bersedia"
                    id="optionRadios2"
                    name="optionsRadios"
                    onChange={changeHandler("pernyataan_1")}
                  />
                 <b> Tidak Bersedia</b>
                </label>
              </div>
            </div>
            <hr/>
            <label>
              Apakah anda memiliki riwayat paparan faktor risiko berikut dalam
              14 hari terakhir? (boleh memilih lebih dari satu) *
            </label>
            <div>
              <input type="checkbox" value="Riwayat kontak erat dengan pasien yang terkonfirmasi atau suspect/terduga terkena COVID-19" onChange={changeHandler("pernyataan_2")}/>
              <label>Riwayat kontak erat dengan pasien yang terkonfirmasi atau suspect/terduga terkena COVID-19</label>
            </div>
            <div>
              <input type="checkbox" value="Bekerja atau mengunjungi fasilitas kesehatan yang berhubungan dengan pasien yang terkonfirmasi atau suspect/terduga terkena COVID-19" onChange={changeHandler("pernyataan_2")}/>
              <label>Bekerja atau mengunjungi fasilitas kesehatan yang berhubungan dengan pasien yang terkonfirmasi atau suspect/terduga terkena COVID-19</label>
            </div>
            <div>
              <input type="checkbox" value="Memiliki demam (> 38 derajat C) atau ada riwayat demam dalam 14 hari terakhir, memiliki riwayat perjalanan ke luar negeri atau kontak dengan orang yang memiliki riwayat perjalanan ke luar negeri" onChange={changeHandler("pernyataan_2")}/>
              <label>Memiliki demam (> 38 derajat C) atau ada riwayat demam dalam 14 hari terakhir, memiliki riwayat perjalanan ke luar negeri atau kontak dengan orang yang memiliki riwayat perjalanan ke luar negeri</label>
            </div>
            <div>
              <input type="checkbox" value="Saya tidak memiliki faktor risiko seperti di atas" onChange={changeHandler("pernyataan_2")}/>
              <label>Saya tidak memiliki faktor risiko seperti di atas</label>
            </div>
            <hr/>
            <label>
              Apakah anda sedang mengalami gejala seperti berikut dalam 14 hari
              terakhir (boleh memilih lebih dari satu) ? *
            </label>
            <div>
              <input type="checkbox" value="Demam/ riwayat demam dalam 14 hari terakhir" onChange={changeHandler("pernyataan_3")}/>
              <label>Demam/ riwayat demam dalam 14 hari terakhir</label>
            </div>
            <div>
              <input type="checkbox" value='Batuk/ pilek/ nyeri tenggorokan' onChange={changeHandler("pernyataan_3")}/>
              <label>Batuk/ pilek/ nyeri tenggorokan</label>
            </div>
            <div>
              <input type="checkbox" value='Sesal Nafas' onChange={changeHandler("pernyataan_3")}/>
              <label>Sesal Nafas</label>
            </div>
            <div>
              <input type="checkbox" value='Saya tidak mengalami keluhan seperti diatas' onChange={changeHandler("pernyataan_3")}/>
              <label>Saya tidak mengalami keluhan seperti diatas</label>
            </div>
            <hr/>
            <label>
              Apakah anda memiliki faktor risiko melakukan perjalanan ke luar
              negeri atau kota-kota terjangkit di Indonesia dalam waktu 14 hari
              terakhir (boleh memilih lebih dari satu)? *
            </label>
            <div>
              <input type="checkbox" value='Jakarta' onChange={changeHandler("pernyataan_4")}/>
              <label>Jakarta</label>
              <br />
              <input type="checkbox" value='Bandung' onChange={changeHandler("pernyataan_4")}/>
              <label>Bandung</label>
              <br />
              <input type="checkbox" value='Yogyakarta' onChange={changeHandler("pernyataan_4")}/>
              <label>Yogyakarta</label>
              <br />
              <input type="checkbox" value='Depok' onChange={changeHandler("pernyataan_4")}/>
              <label>Depok</label>
              <br />
              <input type="checkbox" value='Tangerang' onChange={changeHandler("pernyataan_4")}/>
              <label>Tangerang</label>
              <br />
              <input type="checkbox" value='Bogor' onChange={changeHandler("pernyataan_4")}/>
              <label>Bogor</label>
              <br />
              <input type="checkbox" value='Manado' onChange={changeHandler("pernyataan_4")}/>
              <label>Manado</label>
              <br />
              <input type="checkbox" value='Pontianak' onChange={changeHandler("pernyataan_4")}/>
              <label>Pontianak</label>
              <br />
              <input type="checkbox" value='Solo' onChange={changeHandler("pernyataan_4")}/>
              <label>Solo</label>
              <br />
              <input type="checkbox" value='Denpasar' onChange={changeHandler("pernyataan_4")}/>
              <label>Denpasar</label>
              <br />
              <input type="checkbox" value=' Saya Tidak memiliki riwayat ke luar negeri maupun kota yang
                terjangkit' onChange={changeHandler("pernyataan_3")}/>
              <label>
                Saya Tidak memiliki riwayat ke luar negeri maupun kota yang
                terjangkit
              </label>
              <br />
              <input type="checkbox" />
              <input type="text" class="form-control" onChange={changeHandler("pernyataan_5")}/>
            </div>
            <hr/>
            <label>
              Transportasi umum yang biasa digunakan dalam kurun waktu 14 hari
              terakhir (boleh memilih lebih dari satu) *
            </label>
            <div>
              <input type="checkbox" value="Kendaraan Pribadi" onChange={changeHandler("pernyataan_5")}/>
              <label>Kendaraan Pribadi</label>
              <br />
              <input type="checkbox" value='Kereta Api' onChange={changeHandler("pernyataan_5")}/>
              <label>Kereta Api</label>
              <br />
              <input type="checkbox" value='Bus' onChange={changeHandler("pernyataan_5")}/>
              <label>Bus</label>
              <br />
              <input type="checkbox" value='Angkot' onChange={changeHandler("pernyataan_5")}/>
              <label>Angkot</label>
              <br />
              <input type="checkbox"  value='Taksi' onChange={changeHandler("pernyataan_5")}/>
              <label>Taksi</label>
              <br />
              <input type="checkbox" value='Ojol online (mobil atau motor)' onChange={changeHandler("pernyataan_5")}/>
              <label>Ojek online (mobil atau motor)</label>
              <br />
              <input type="checkbox" />
              <input type="text" class="form-control" onChange={changeHandler("pernyataan_5")}/>
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <div class="btn-group">
            <button class="btn btn-success" onClick={onSave}>Ya</button>
            <button class="btn btn-info" onClick={cancel}>
              Tidak
            </button>
          </div>
        </Modal.Footer>
      </Modal>
    );
  }
}
export default FormInput;
