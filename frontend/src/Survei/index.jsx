import React from "react";
import surveiService from "../Service/Survei";
import FormInput from "./formInput";
import Table from "react-bootstrap/Table";
import Modal from 'react-bootstrap/Modal';

class Survei extends React.Component {
  model = {
    nama: "",
    email: "",
    umur: "",
    no_hp: "",
    kelurahan: "",
    kecamatan: "",
    kabupaten: "",
    fakultas_id: "",
    status_id: "",
    pernyataan_1: "",
    pernyataan_2: [],
    pernyataan_3: [],
    pernyataan_4: [],
    pernyataan_5: [],
  };

  constructor() {
    super();
    this.state = {
      model: this.model,
      open_create: false,
      errors: {},
      list_status: [],
      list_fakultas: [],
      list_kota: [],
      list_tp: [],
      mode: "",
      data: [],
      open_delete:false
    };
  }

  getStatus = async () => {
    const respon = await surveiService.getStatus();
    this.setState({
      list_status: respon.result,
    });
  };

  getFakultas = async () => {
    const respon = await surveiService.getFakultas();
    this.setState({
      list_fakultas: respon.result,
    });
  };

  getKota = async () => {
    const respon = await surveiService.getKota();
    this.setState({
      list_kota: respon.result,
    });
  };

  getTP = async () => {
    const respon = await surveiService.getTP();
    this.setState({
      list_tp: respon.result,
    });
  };

  selectHandler_status = (name) => ({ target: { value } }) => {
    this.setState({
      model: {
        ...this.state.model,
        [name]: value,
      },
    });
  };

  selectHandler_fakultas = (name) => ({ target: { value } }) => {
    this.setState({
      model: {
        ...this.state.model,
        [name]: value,
      },
    });
  };

  hendleOpen = () => {
    this.getStatus();
    this.getFakultas();
    this.setState({
      open_create: true,
      mode: "create",
    });
  };

  cancel = () => {
    this.setState({
      open_create: false,
    });
  };

  cancel_delete = () => {
    this.setState({
      open_delete: false,
    });
  };

  changeHandler = (name) => ({ target: { value } }) => {
    this.setState({
      model: {
        ...this.state.model,
        [name]: value,
      },
    });
  };

  onSave = async () => {
    const { model, mode } = this.state;

    if (mode === "create") {
      const respon = await surveiService.save(model);
      if (respon.success) {
        alert(respon.result);
      } else {
        alert("error" + respon.result);
      }
    }
  };

  loadlist = async () => {
    const respon = await surveiService.getList();
    if (respon.success) {
      this.setState({
        data: respon.result,
      });
    }
  };

  componentDidMount() {
    this.loadlist();
  }

  hendlerDel = async (id) => {
    const respon = await surveiService.getbyid(id);
    if (respon.success) {
        this.setState({
            open_delete: true,
            model: respon.result[0]
        })
    }
    else {
        alert(respon.result);
    }

}

    sureDelete = async (item) => {
        const { model } = this.state;
        const respon = await surveiService.delete(model);

        if (respon.success) {
            alert('Sukses' + respon.result)
            this.setState({
                open_delete: false,
            })
        }
    }

  render() {
    const {
      open_create,
      list_status,
      list_fakultas,
      list_kota,
      list_tp,
      model,
      data,
      open_delete
    } = this.state;
    return (
      <div>
        <div className="btn-group pull-right">
          <button
            type="button"
            className="btn btn-primary pull-right"
            onClick={this.hendleOpen}
          >
            <i class="fa fa-plus-circle"> Create</i>
          </button>
          <br />
        </div>

        <FormInput
          cancel={this.cancel}
          open_create={open_create}
          list_status={list_status}
          list_fakultas={list_fakultas}
          list_kota={list_kota}
          list_tp={list_tp}
          model={model}
          changeHandler={this.changeHandler}
          selectHandler_fakultas={this.selectHandler_fakultas}
          selectHandler_status={this.selectHandler_status}
          onSave={this.onSave}
        />

        <Modal show={open_delete} style={{ opacity: 1 }}>
          <Modal.Body>Apakah anda akan menghapus data ini {data.nama_lengkap} ?</Modal.Body>
          <Modal.Footer>
            <div class="btn-group">
              <button
                class="btn btn-success"
                onClick = {this.sureDelete(data.id)}
              >
                Ya
              </button>
              <button class="btn btn-info" onClick={this.cancel_delete}>
                Tidak
              </button>
            </div>
          </Modal.Footer>
        </Modal>

        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Nama</th>
              <th>Fakultas</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {data.map((data) => {
              return (
                <tr>
                  <td>{data.nama_lengkap}</td>
                  <td>{data.fakultas}</td>
                  <td>{data.status}</td>
                  <td>
                    <div className="btn-group">
                      <button className="btn btn-primary">Edit</button>
                      <button className="btn btn-danger" onClick={() => this.hendlerDel(data.id)}>Delete</button>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </div>
    );
  }
}

export default Survei;
