module.exports = exports = (server, pool) => {
    server.get('/api/get_status', (req, res) => {

        pool.query(`Select * from public."status" where "is_delete" = 'false'`, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
    });

    server.get('/api/get_survei_list', (req, res) => {

        pool.query(`select d."id", a."nama_lengkap", b."fakultas", c."status" from biodata as a
        join "fakultas" as b on a.fakultas_id = b.id
        join "status" as c on a.status_id = c.id
        join "survei" as d on a.id = d.biodata_id`, (error, result) => {
            if (error) {    
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
    });

    server.get('/api/getdata/:id', (req, res) => {
        const id = req.params.id;
        const query = `select d."id", a."nama_lengkap", b."fakultas", c."status", e."kelurahan",
        e."kecamatan", e."kabupaten", d."pernyataan_1",  d."pernyataan_2",  d."pernyataan_3",
        d."pernyataan_4",  d."pernyataan_5"  from biodata as a
        join "alamat" as e on a.id = e.biodata_id
        join "fakultas" as b on a.fakultas_id = b.id
        join "status" as c on a.status_id = c.id
        join "survei" as d on a.id = d.biodata_id where d."id" = ${id}`;

        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows[0]
                })
            }
        })
    })

    server.get('/api/get_fakultas', (req, res) => {

        pool.query(`Select * from public."fakultas" where "is_delete" = 'false'`, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
    });

    server.get('/api/get_transportasi', (req, res) => {

        pool.query(`Select * from public."fakultas"`, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
    });

    server.get('/api/get_kota', (req, res) => {

        pool.query(`Select * from public."kota"`, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
    });

    server.post('/api/create_survei', (req, res) => {
        console.log(req.body)

        const {
            nama,
            email,
            umur,
            no_hp,
            kelurahan,
            kecamatan,
            kabupaten,
            fakultas_id,
            status_id,
            pernyataan_1,
            pernyataan_2,
            pernyataan_3,
            pernyataan_4,
            pernyataan_5
        } = req.body;

        let detQuery = `select "biodata_id" from insert1`;

        let query = `WITH insert1 AS ( ` + `INSERT INTO "biodata" ("created_by", "created_date", "nama_lengkap", "email", "umur", "no_hp", "fakultas_id", "status_id", "is_delete")
        VALUES (1, current_timestamp, '${nama}', '${email}', '${umur}', '${no_hp}', ${fakultas_id}, ${status_id}, false) RETURNING "id" AS "biodata_id" ` + `) ,
        insert2 AS ( ` + `INSERT INTO "alamat" ("created_by", "created_date", "kelurahan", "kecamatan", "kabupaten", "biodata_id", "is_delete")
        VALUES (1, current_timestamp, '${kelurahan}', '${kecamatan}', '${kabupaten}', (${detQuery}), false)` + `),
        insert3 AS (` + `INSERT INTO "survei" ("created_by", "created_date", "pernyataan_1", "pernyataan_2",
        "pernyataan_3", "pernyataan_4", "pernyataan_5", "biodata_id", "is_delete")
        VALUES (1, current_timestamp, '${pernyataan_1}', '${pernyataan_2}', '${pernyataan_3}', '${pernyataan_4}', '${pernyataan_5}', (${detQuery}), false ) ` + `) SELECT * FROM insert1`;

        console.log(query)
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(201, {
                    success: true,
                    result: "Berhasil Disimpan"
                })
            }
        })
    })

    server.put('/api/ubah_survei/:id', (req, res) => {
        const id = req.params.id;

        const {
            nama,
            email,
            umur,
            no_hp,
            kelurahan,
            kecamatan,
            kabupaten,
            fakultas_id,
            status_id,
            pernyataan_1,
            pernyataan_2,
            pernyataan_3,
            pernyataan_4,
            pernyataan_5
        } = req.body;

        let detQuery = `select "biodata_id" from update1`;


        let query = `WITH  update1 AS(` +
            `UPDATE public."biodata" SET "biodata" SET "modified_by" = 1, "modified_date" = current_timestamp, "nama" = '${nama},
            "email" = '${email}', "umur" = '${umur}', "no_hp" = '${no_hp}', "fakultas_id" = ${fakultas_id}, "status_id" = ${status_id}
            WHERE "id" = ${id} RETURING "id" AS "biodata_id" ` + `), update2 AS (` + `UPDATE public."alamat"
        SET "modified_by" = 1, "modified_date" = current_timestamp, "kelurahan" = '${kelurahan}',
        "kecamatan" = '${kecamatan}', "kabupaten" = '${kabupaten}' WHERE "biodata_id" = (${detQuery}) ` + `),
        update3 AS (` + `UPDATE public."survei" SET "modified_by" = 1, "modified_date" = current_timestamp, "pernyataan_1" = '${pernyataan_1}'
        , "pernyataan_2" = '${pernyataan_2}', "pernyataan_3" = '${pernyataan_3}', "pernyataan_4" = '${pernyataan_4}', "pernyataan_5" = '${pernyataan_5}', biodata_id= (${detQuery})  ` + `) Select * FROM update1`;

        console.log(query)
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(201, {
                    success: true,
                    result: "Berhasil Diupdate"
                })
            }
        })
    })

    server.put('/api/delete_survei/:id', (req, res) => {
        const id = req.params.id;

        let detQuery = `select "biodata_id" from update1`;


        let query = `WITH  update1 AS(` +
            `UPDATE public."biodata" SET "biodata" SET "delete_by" = 1, "delete_date" = current_timestamp, 
            "is_delete" = true WHERE "id" = ${id} RETURING "id" AS "biodata_id" ` + `), update2 AS (` + `UPDATE public."alamat"
            SET "delete_by" = 1, "delete_date" = current_timestamp, 
            "is_delete" = true WHERE "biodata_id" = (${detQuery}) ` + `),
            update3 AS (` + `UPDATE public."survei"
            SET "delete_by" = 1, "delete_date" = current_timestamp, 
            "is_delete" = true WHERE "biodata_id" = (${detQuery})` + `) Select * FROM update1`;

        console.log(query)
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(201, {
                    success: true,
                    result: "Berhasil hapus"
                })
            }
        })
    })

    server.post('/api/create_fakultas', (req, res) => {
        const {
            fakultas,
            jurusan
        } = req.body;

        let Qjurusan = jurusan != "" ? `'${jurusan}'` : null;


        var query = `INSERT INTO "fakultas"(
            "created_by", "created_date" "fakultas", "jurusan")
            VALUES (1, current_timestamp, '${fakultas}', ${Qjurusan} )`;

        console.log(query)
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(201, {
                    success: true,
                    result: "Berhasil Disimpan"
                })
            }
        })

    });

    server.post('/api/create_status', (req, res) => {
        const {
            status
        } = req.body;

        var query = `INSERT INTO "status"(
            "created_by", "created_date" "status")
            VALUES (1, current_timestamp, '${status}' )`;

        console.log(query)
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(201, {
                    success: true,
                    result: "Berhasil Disimpan"
                })
            }
        })

    });
}