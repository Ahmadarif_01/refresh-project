const Restify = require("restify");
const corsMidWare=require('restify-cors-middleware')
const server = Restify.createServer({
    name: "Server Web API",
    version: "1.0.0"
})


server.use(Restify.plugins.bodyParser({
    mapParams: false
}))
//configuration untuk connect ke database
global.config = require('./config/connection');


//config untuk cors ke react
const cors=corsMidWare({
    origins:['http://localhost:3000'],
    allowHeaders :['x-access-token'],
    exposeHeader:[]
})
server.pre(cors.preflight);
server.use(cors.actual);
//end config cors

//import service

require('./service/survei')(server, global.config.pool);


server.listen(4000, function () {
    console.log(server.name, "Success Listen . . .");

});