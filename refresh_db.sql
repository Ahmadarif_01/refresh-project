--
-- PostgreSQL database dump
--

-- Dumped from database version 11.7
-- Dumped by pg_dump version 11.7

-- Started on 2020-07-09 17:20:26

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 200 (class 1259 OID 24672)
-- Name: alamat; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.alamat (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_date timestamp with time zone NOT NULL,
    modified_by bigint,
    modified_date timestamp with time zone,
    delete_by bigint,
    delete_date timestamp with time zone,
    is_delete boolean NOT NULL,
    kelurahan character varying(200),
    kecamatan character varying(200),
    kabupaten character varying(200),
    biodata_id bigint NOT NULL
);


ALTER TABLE public.alamat OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 24678)
-- Name: alamat_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.alamat ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.alamat_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999
    CACHE 1
);


--
-- TOC entry 199 (class 1259 OID 24667)
-- Name: biodata; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.biodata (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_date time with time zone NOT NULL,
    modified_by bigint,
    modified_date timestamp with time zone,
    delete_by bigint,
    delete_date timestamp with time zone,
    is_delete boolean NOT NULL,
    nama_lengkap character varying(150),
    email character varying(100) NOT NULL,
    umur character varying(5),
    no_hp character varying(15),
    status_id bigint NOT NULL,
    fakultas_id bigint NOT NULL
);


ALTER TABLE public.biodata OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 24665)
-- Name: biodata_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.biodata ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.biodata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999
    CACHE 1
);


--
-- TOC entry 203 (class 1259 OID 24682)
-- Name: fakultas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fakultas (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_date timestamp with time zone NOT NULL,
    modified_by bigint,
    modified_date timestamp with time zone,
    delete_by bigint,
    delete_date timestamp with time zone,
    is_delete boolean NOT NULL,
    fakultas character varying(200),
    jurusan character varying(200)
);


ALTER TABLE public.fakultas OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 24680)
-- Name: fakultas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.fakultas ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.fakultas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999
    CACHE 1
);


--
-- TOC entry 205 (class 1259 OID 24689)
-- Name: status; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.status (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_date timestamp with time zone NOT NULL,
    modified_by bigint,
    modified_date timestamp with time zone,
    delete_by bigint,
    delete_date timestamp with time zone,
    is_delete boolean NOT NULL,
    status character varying(150)
);


ALTER TABLE public.status OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 24687)
-- Name: status_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.status ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999
    CACHE 1
);


--
-- TOC entry 197 (class 1259 OID 24660)
-- Name: survei; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.survei (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_date timestamp with time zone NOT NULL,
    modified_by bigint,
    modified_date timestamp with time zone,
    delete_by bigint,
    delete_date timestamp with time zone,
    is_delete boolean NOT NULL,
    pernyataan_1 character varying(15),
    pernyataan_2 text,
    pernyataan_3 text,
    pernyataan_4 text,
    pernyataan_5 text,
    biodata_id bigint NOT NULL
);


ALTER TABLE public.survei OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 24658)
-- Name: survei_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.survei ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.survei_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999
    CACHE 1
);


--
-- TOC entry 2844 (class 0 OID 24672)
-- Dependencies: 200
-- Data for Name: alamat; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.alamat (id, created_by, created_date, modified_by, modified_date, delete_by, delete_date, is_delete, kelurahan, kecamatan, kabupaten, biodata_id) FROM stdin;
1	1	2020-07-09 16:38:37.409237+07	\N	\N	\N	\N	f	kopo	margahayu	bandung	3
2	1	2020-07-09 17:04:20.702199+07	\N	\N	\N	\N	f	kopo	kopo	kebumen	4
\.


--
-- TOC entry 2843 (class 0 OID 24667)
-- Dependencies: 199
-- Data for Name: biodata; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.biodata (id, created_by, created_date, modified_by, modified_date, delete_by, delete_date, is_delete, nama_lengkap, email, umur, no_hp, status_id, fakultas_id) FROM stdin;
3	1	16:38:37.409237+07	\N	\N	\N	\N	f	aa	aa@gmail.com	23	0988912	5	7
4	1	17:04:20.702199+07	\N	\N	\N	\N	f	bb	bb@gmail.com	23	089921	5	6
\.


--
-- TOC entry 2847 (class 0 OID 24682)
-- Dependencies: 203
-- Data for Name: fakultas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.fakultas (id, created_by, created_date, modified_by, modified_date, delete_by, delete_date, is_delete, fakultas, jurusan) FROM stdin;
2	1	2020-07-09 10:13:37.041597+07	\N	\N	\N	\N	f	Pertanian	\N
3	1	2020-07-09 10:13:45.321713+07	\N	\N	\N	\N	f	Biologi	\N
4	1	2020-07-09 10:13:58.610724+07	\N	\N	\N	\N	f	Ekonomi dan Bisnis	\N
5	1	2020-07-09 10:14:07.56137+07	\N	\N	\N	\N	f	Peternakan	\N
6	1	2020-07-09 10:14:14.420898+07	\N	\N	\N	\N	f	Hukum	\N
7	1	2020-07-09 10:14:29.507421+07	\N	\N	\N	\N	f	Ilmu Sosial dan Politik	\N
8	1	2020-07-09 10:14:46.923423+07	\N	\N	\N	\N	f	Kedokteran	\N
9	1	2020-07-09 10:14:55.468645+07	\N	\N	\N	\N	f	Teknik	\N
10	1	2020-07-09 10:15:07.761423+07	\N	\N	\N	\N	f	Ilmu ilmu Kesehatan	\N
11	1	2020-07-09 10:15:17.393869+07	\N	\N	\N	\N	f	MIPA	\N
12	1	2020-07-09 10:15:32.750858+07	\N	\N	\N	\N	f	Program Pascasarjana	\N
13	1	2020-07-09 10:15:43.277653+07	\N	\N	\N	\N	f	Ilmu Budaya	\N
14	1	2020-07-09 10:16:13.244427+07	\N	\N	\N	\N	f	Perikanan dan Ilmu Kelautan	\N
\.


--
-- TOC entry 2849 (class 0 OID 24689)
-- Dependencies: 205
-- Data for Name: status; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.status (id, created_by, created_date, modified_by, modified_date, delete_by, delete_date, is_delete, status) FROM stdin;
1	1	2020-07-09 10:21:22.994826+07	\N	\N	\N	\N	f	Dosen
2	1	2020-07-09 10:21:32.222019+07	\N	\N	\N	\N	f	Tenaga Pendidikan
3	1	2020-07-09 10:21:54.256022+07	\N	\N	\N	\N	f	Cleaning Service (CS)
4	1	2020-07-09 10:22:06.120704+07	\N	\N	\N	\N	f	Security
5	1	2020-07-09 10:22:13.339267+07	\N	\N	\N	\N	f	Mahasiswa
\.


--
-- TOC entry 2841 (class 0 OID 24660)
-- Dependencies: 197
-- Data for Name: survei; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.survei (id, created_by, created_date, modified_by, modified_date, delete_by, delete_date, is_delete, pernyataan_1, pernyataan_2, pernyataan_3, pernyataan_4, pernyataan_5, biodata_id) FROM stdin;
1	1	2020-07-09 16:38:37.409237+07	\N	\N	\N	\N	f	Bersedia	Memiliki demam (> 38 derajat C) atau ada riwayat demam dalam 14 hari terakhir, memiliki riwayat perjalanan ke luar negeri atau kontak dengan orang yang memiliki riwayat perjalanan ke luar negeri	 Saya Tidak memiliki riwayat ke luar negeri maupun kota yang\r terjangkit	Denpasar	Ojol online (mobil atau motor)	3
2	1	2020-07-09 17:04:20.702199+07	\N	\N	\N	\N	f	Bersedia	Memiliki demam (> 38 derajat C) atau ada riwayat demam dalam 14 hari terakhir, memiliki riwayat perjalanan ke luar negeri atau kontak dengan orang yang memiliki riwayat perjalanan ke luar negeri	Sesal Nafas	Bandung	Kereta Api	4
\.


--
-- TOC entry 2855 (class 0 OID 0)
-- Dependencies: 201
-- Name: alamat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.alamat_id_seq', 2, true);


--
-- TOC entry 2856 (class 0 OID 0)
-- Dependencies: 198
-- Name: biodata_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.biodata_id_seq', 4, true);


--
-- TOC entry 2857 (class 0 OID 0)
-- Dependencies: 202
-- Name: fakultas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.fakultas_id_seq', 14, true);


--
-- TOC entry 2858 (class 0 OID 0)
-- Dependencies: 204
-- Name: status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.status_id_seq', 5, true);


--
-- TOC entry 2859 (class 0 OID 0)
-- Dependencies: 196
-- Name: survei_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.survei_id_seq', 2, true);


--
-- TOC entry 2714 (class 2606 OID 24671)
-- Name: biodata biodata_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.biodata
    ADD CONSTRAINT biodata_pkey PRIMARY KEY (id);


--
-- TOC entry 2716 (class 2606 OID 24686)
-- Name: fakultas fakultas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fakultas
    ADD CONSTRAINT fakultas_pkey PRIMARY KEY (id);


--
-- TOC entry 2718 (class 2606 OID 24693)
-- Name: status status_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.status
    ADD CONSTRAINT status_pkey PRIMARY KEY (id);


--
-- TOC entry 2712 (class 2606 OID 24664)
-- Name: survei survei_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.survei
    ADD CONSTRAINT survei_pkey PRIMARY KEY (id);


-- Completed on 2020-07-09 17:20:29

--
-- PostgreSQL database dump complete
--

